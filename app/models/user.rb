class User < ApplicationRecord
  include Discard::Model
  default_scope -> { kept }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :role
  has_many :orders

  validates_confirmation_of :password_confirmation
  validates_presence_of  :email, :first_name, :password
  validates_uniqueness_of :email

  self.per_page = 10
  def full_name
   "#{try(:first_name)} #{try(:last_name)}".to_s
  end

  def active_for_authentication?
    super && !discarded_at
  end
end
