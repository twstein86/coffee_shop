class Menu < ApplicationRecord
	include Discard::Model
	default_scope -> { kept }
	before_create :set_available_quantity
	before_update :update_available_quantity

	validates_presence_of :name
	validates :quantity, presence: true, numericality: { only_integer: true }
	validates :price, presence: true, numericality: true 

	has_many :menu_orders
	has_many :orders, through: :menu_orders

	self.per_page = 10

	private
		def set_available_quantity
			self.available_quantity = self.quantity			
		end

		def update_available_quantity
			if quantity_changed?
				quantity_update = quantity - quantity_was
				self.available_quantity = self.available_quantity + quantity_update
			end
		end
end
