class Order < ApplicationRecord
	self.per_page = 10

	has_many :menu_orders
	has_many :menus, through: :menu_orders
	belongs_to :user
	validates_presence_of :customer_name, :customer_ph_number
end
