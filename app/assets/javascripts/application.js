// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require materialize
//= require_tree .

$( document ).on('turbolinks:load', function() {
  $('form').on('click', '.add_order_items', function(event){
    var active = document.querySelector("#items_table");
    active.classList.remove("hide");
    var e = document.getElementById("menu_items");
    var item_id = e.options[e.selectedIndex].value;
    $.ajax({
      type: 'GET',
      url: '/orders/get_menu',
      data: { menu_id: item_id },
      dataType: "json",
      complete: function() {},
      success: function(data, textStatus, xhr){
        if (data.status == 200) {
          var menu_id = data.menu.id;
          var available_quantity = data.menu.available_quantity;
          if ($("#item_menu_ids").val() == "") {
            var elems = [];
            elems.push(menu_id);
            $('#item_menu_ids').val(JSON.stringify(elems));
          } else {
            var value = $('#item_menu_ids').val();
            value = JSON.parse(value);
            var idx = $.inArray(menu_id, value);
            if ($.inArray(menu_id, value) == -1 ){
              var elems = value;
              elems.push(menu_id);
              $('#item_menu_ids').val(JSON.stringify(elems));
            } else {
              alert("Item is already in List.")
              return event.preventDefault();
            }
          }
          var tax = (data.menu.price/10);
          var total_price = data.menu.price + tax
          var append_div = "<tr id='item_row_"+menu_id+"'> <input type='hidden' name='menu_ids[]' value='"+ menu_id +"'> <input type='hidden' name='item_quantity[]' value='1' id='item_quantity_"+ menu_id +"'> <td> <a href='javascript:void(0)' onclick='remove_row("+menu_id+")'> <i class='material-icons'>delete</i> </a></td> <td>" + data.menu.name + "</td> <td>" + available_quantity + "</td> <td class='item_price'>$" + data.menu.price + "</td> <td id='item_tax_"+menu_id+"' class='item_tax'>$" + tax + "</td> <td id='item_total_price_"+menu_id+"' class='total_price'>$" + total_price + "</td>  <td> <input type='number' min='1' step='1' value='1' class='number_of_items' id='input_menu_item_"+ menu_id +"' onchange='calculatePrice(" + menu_id + ", " + data.menu.price + ", "+ available_quantity +")'> </td> </tr>"
          $(".append_order_items").append(append_div);
          calculateFinalPrice();        
        }
      },
      error: function() {
        alert("Ajax error!")
      }
    });
    return event.preventDefault();
  });
});


function calculatePrice(id, price, available_quantity){
  var input_value = $("#input_menu_item_"+id).val();
  if (input_value > available_quantity) {
    alert(input_value + " items are not avaiable.");
  } else {
    var price = (price * input_value);
    var tax = price/10;
    var total_price = price + tax;
    $("#item_tax_"+id).text(tax.toFixed(2));
    $("#item_quantity_"+id).val(input_value);
    $("#item_total_price_"+id).text(total_price.toFixed(2));
    calculateFinalPrice();
  }
}

function calculateFinalPrice() {
  var price = 0;
  $(".item_price").each(function() {
    price += parseFloat($(this).text().replace("$", ""));
  });
  $("#final_price").text("$"+price.toFixed(2));
  
  var tax = 0;
  $(".item_tax").each(function() {
    tax += parseFloat($(this).text().replace("$", ""));
  });
  $("#final_tax").text("$"+tax.toFixed(2));
  $("#order_tax_amount").val(tax.toFixed(2));
  
  var total_price = 0;
  $(".total_price").each(function() {
    total_price += parseFloat($(this).text().replace("$", ""));
  });
  $("#final_total_price").text("$"+total_price.toFixed(2));
  $("#order_price").val(total_price.toFixed(2));

  var number_of_items = 0;
  $(".number_of_items").each(function() {
    number_of_items += parseFloat($(this).val());
  });
  $("#final_number_of_items").text(number_of_items);
  $("#order_number_of_items").val(number_of_items);
}

function remove_row(id) {
  $("#item_row_"+id).remove();
  calculateFinalPrice();
  var value = $('#item_menu_ids').val();
  value = JSON.parse(value);
  var arr = value;
  var idx = $.inArray(id, arr);
  arr.splice(idx, 1);
  $('#item_menu_ids').val(JSON.stringify(arr));
}