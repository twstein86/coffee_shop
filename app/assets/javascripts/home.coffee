# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  # Toast for error and notice accorinding to screen size
  if $(window).width() >= 992
    $('#error_explanation ul li').each ->
      M.toast
        html: $(this).text()
        classes: 'red rounded'
    if $('.notice').text().length
      M.toast
        html: $('.notice').text()
        classes: 'rounded blue'
    if $('.alert').text().length
      M.toast
        html: $('.alert').text()
        classes: 'red rounded'
  else
    $('#error_explanation ul li').each ->
      M.toast
        html: $(this).text()
        classes: 'red'
    if $('.notice').text().length
      M.toast 
      	html: $('.notice').text()
      	classes: 'blue'
    if $('.alert').text().length
      M.toast
        html: $('.alert').text()
        classes: 'red'
  $('select').formSelect();
  $('.datepicker').datepicker();
  M.updateTextFields();
  return

  