class MenusController < ApplicationController
  before_action :check_role
  before_action :set_menu_item, only: [:show, :edit, :update, :destroy]
 
  def index
    @menus = Menu.paginate(:page => params[:page]).order(id: :asc)
    @menus_count = @menus.count/Menu.per_page
    unless @menus.count%Menu.per_page == 0
      @menus_count = @menus_count + 1
    end
  end

 
  def show
  end

  def new
    @menu = Menu.new
  end

  def edit
  end

  def create
    @menu = Menu.new(menu_item_params)

    respond_to do |format|
      if @menu.save
        format.html { redirect_to menus_path, notice: 'New Menu item is created successfuly.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end

  end

  def update
    respond_to do |format|
      if @menu.update(menu_item_params)
        format.html { redirect_to menus_path, notice: 'Menu Item is updated successfully.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @menu.discard
    respond_to do |format|
      format.html { redirect_to menus_path(page: params[:page]), notice: 'Menu Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def check_role
      if current_user.role.name == "Sales"
        redirect_to root_path, notice: "You have not permission to access Menu Items."
      end
    end

    def set_menu_item
      @menu = Menu.find(params[:id])
    end

    def menu_item_params
      params.require(:menu).permit(:name, :price, :quantity)
    end
end
