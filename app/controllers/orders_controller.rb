class OrdersController < ApplicationController
  before_action :check_role, only: [:new, :create]

  def index
    if current_user.role.name == "Sales"
      @orders = current_user.orders #.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day)
    else
      @sales_persons = User.where(role_id: 2)
      @orders = Order.all #.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day)
    end    
    @total_items = @orders.sum(:number_of_items)
    @total_price = @orders.sum(:price)
  end

  def filter_data
    if current_user.role.name == "Sales"
      @orders = current_user.orders.where(created_at: params["start_date"].to_datetime.beginning_of_day..params["end_date"].to_datetime.end_of_day)
    elsif params['sales_user'].present?
      @sales_persons = User.where(role_id: 2)
      @orders = Order.where(created_at: params["start_date"].to_datetime.beginning_of_day..params["end_date"].to_datetime.end_of_day, user_id: params['sales_user'])
    else
      @sales_persons = User.where(role_id: 2)
      @orders = Order.where(created_at: params["start_date"].to_datetime.beginning_of_day..params["end_date"].to_datetime.end_of_day)
    end
    @total_items = @orders.sum(:number_of_items)
    @total_price = @orders.sum(:price)
  end

  def new
    @order = current_user.orders.new
    @menus = Menu.where("available_quantity > ?", 0)
  end

  def create
    @order = current_user.orders.new(order_params)
    respond_to do |format|
      if @order.save
        params["menu_ids"].each_with_index do |menu, index|
          menu_item = Menu.find menu
          menu_item.update_attributes(available_quantity: (menu_item.available_quantity - params["item_quantity"][index].to_i))
          MenuOrder.create(menu_id: menu_item.id, order_id: @order.id, item_quantity: params["item_quantity"][index].to_i, price: (menu_item.price * params["item_quantity"][index].to_i))
        end
        format.html { redirect_to orders_path, notice: 'New Order is created successfuly.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    @order = Order.find params[:id]
  end

  def get_menu
    respond_to do |format|
      format.html
      format.json { render :json => { menu: Menu.find(params[:menu_id]), status: 200 }}
    end
  end

  private
    def check_role
      if current_user.role.name == "Manager"
        redirect_to root_path, notice: "You have not permission to create order."
      end
    end

    def order_params
      params.require(:order).permit(:customer_name, :customer_ph_number,  :price, :number_of_items, :tax_amount)
    end
end
