class SalesUsersController < ApplicationController
  before_action :check_role
  before_action :set_sales_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.where(role_id: 2).paginate(:page => params[:page]).order(id: :asc)
    # @users = User.all.order(id: :asc)
    @users_count = @users.count/User.per_page
    unless @users.count%User.per_page == 0
      @users_count = @users_count + 1
    end
  end

 
  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(sales_users_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to sales_users_path, notice: 'New Sales Person is created successfuly.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end

  end

  def update
    respond_to do |format|
      if @user.update(sales_users_params)
        format.html { redirect_to sales_users_path, notice: 'Sales User is updated successfully.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.discarded_at = Time.now
    @user.save(validate: false)
    respond_to do |format|
      format.html { redirect_to sales_users_path(page: params[:page]), notice: 'Sales User is successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def check_role
      if current_user.role.name == "Sales"
        redirect_to root_path, notice: "You have not permission to access Menu Items."
      end
    end

    def set_sales_user
      @user = User.find(params[:id])
    end

    def sales_users_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :role_id, :pay_scale)
    end
end
