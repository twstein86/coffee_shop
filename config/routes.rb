Rails.application.routes.draw do
  root 'orders#index'
  devise_for :users, path_names:{
    :sign_in => 'login',
    :sign_out => 'logout' 
  }, 
  controllers: {
    sessions: 'users/sessions',
    registrations: "users/registrations",
    passwords: "users/passwords"
  }
   
  resources :menus
  resources :orders do
    collection do
      get "get_menu"
      post "filter_data"
    end
  end
  resources :sales_users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
