require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'valid user' do
  	role = Role.create(name:"test")
    user = User.new(first_name: 'John', email: 'john@example.com',password: '12345678', password_confirmation: '12345678',role_id: role.id)
    assert user.valid?
  end

  test 'invalid without first_name' do
    user = User.new(email: 'john@example.com')
    refute user.valid?, 'user is valid without a first_name'
    assert_not_nil user.errors[:first_name], 'no validation error for first_name present'
  end

  test 'invalid without email' do
    user = User.new(first_name: 'John')
    refute user.valid?
    assert_not_nil user.errors[:email]
  end

end
