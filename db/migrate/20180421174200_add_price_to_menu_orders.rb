class AddPriceToMenuOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :menu_orders, :price, :float
  end
end
