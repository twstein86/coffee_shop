class AddDiscardToMenus < ActiveRecord::Migration[5.2]
  def change
    add_column :menus, :discarded_at, :datetime
    add_index :menus, :discarded_at
  end
end
