class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.float :price
      t.integer :number_of_items
      t.string :customer_name
      t.string :customer_ph_number
      t.float :tax_amount
      t.integer :user_id

      t.timestamps
    end
  end
end
