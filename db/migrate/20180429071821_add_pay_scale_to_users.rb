class AddPayScaleToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :pay_scale, :float, default: 0.0
  end
end
