class CreateMenuOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :menu_orders do |t|
      t.integer :menu_id
      t.integer :order_id
      t.integer :item_quantity

      t.timestamps
    end
  end
end
