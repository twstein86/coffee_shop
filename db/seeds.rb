# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Role.create!([{
		name: "Manager"
	},
	{
		name: "Sales"
	}
])

User.create!([{
		first_name: "Sale", last_name: "Person 1", email: "sales1@example.com", password: "password", password_confirmation: "password", role_id: 2
	},
	{
		first_name: "Sale", last_name: "Person 2", email: "sales2@example.com", password: "password", password_confirmation: "password", role_id: 2
	},
	{
		first_name: "Manager", last_name: "Person 1", email: "manager1@example.com", password: "password", password_confirmation: "password", role_id: 1
	}
])

Menu.create!([{
		name: "Espresso / Americano", quantity: 20, price: 2.50 
	},{
		name: "Macchiata", quantity: 25, price: 2.50
	},{
		name: "Latte", quantity: 20, price: 3.50
	},{
		name: "Cappuccino", quantity: 15, price: 3.50
	},{
		name: "Mocha", quantity: 10, price: 4.00
	},{
		name: "Extra Shot", quantity: 20, price: 1.00 
	},{
		name: "Drip", quantity: 30, price: 2.0 
	},{
		name: "French Press", quantity: 15, price: 3.50
	},{
		name: "vacuum Pot", quantity: 12, price: 5.0
	},{
		name: "1lb Beans", quantity: 15, price: 14.0
	}
])

